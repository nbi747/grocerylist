Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  get  '/about',   to: 'static_pages#about'
  get  'static_pages/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'
  get  'static_pages/contact', to: 'static_pages#contact'

  get '/sign_in', to: 'users#sign_in'

  resources :homegroups do
    resources :items
  end

  resources :invites

  devise_for :users, controllers: { sessions: 'users/sessions' }

  root to: 'static_pages#about'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
