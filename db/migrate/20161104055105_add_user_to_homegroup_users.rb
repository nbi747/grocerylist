class AddUserToHomegroupUsers < ActiveRecord::Migration[5.0]
  def change
    add_reference :homegroup_users, :user, foreign_key: true
  end
end
