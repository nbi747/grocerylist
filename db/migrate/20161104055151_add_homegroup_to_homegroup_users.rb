class AddHomegroupToHomegroupUsers < ActiveRecord::Migration[5.0]
  def change
    add_reference :homegroup_users, :homegroup, foreign_key: true
  end
end
