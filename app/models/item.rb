class Item < ApplicationRecord
  belongs_to :homegroup
  attr_accessor :homegroup_id
  validates :item_name, presence: true

end
