class HomegroupsController < ApplicationController
  before_action :set_homegroup, only: [:show, :edit, :update]


  # GET /homegroups
  # GET /homegroups.json
  def index
    @homegroups = current_user.homegroups.all
  end

  # GET /homegroups/1
  # GET /homegroups/1.json
  def show
    @homegroup = Homegroup.find(params[:id])
    @invite = @homegroup.invites.build
  end

  # GET /homegroups/new
  # not sure about the .new part
  def new
    @homegroup = current_user.homegroups.new
  end

  # GET /homegroups/1/edit
  def edit
    @homegroup = Homegroup.find(params[:id])

  end

  # POST /homegroups
  # POST /homegroups.json
  def create
    def homegroup_params
      params.require(:homegroup).permit(:name)
    end
    @homegroup = current_user.homegroups.create(homegroup_params)


    respond_to do |format|
      if @homegroup.save
        format.html { redirect_to @homegroup, notice: 'Homegroup was successfully created.' }
        format.json { render :show, status: :created, location: @homegroup }
      else
        format.html { render :new }
        format.json { render json: @homegroup.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /homegroups/1
  # PATCH/PUT /homegroups/1.json
  def update
    def homegroup_params
      params.require(:homegroup).permit(:name)
    end
    respond_to do |format|
      if @homegroup.update(homegroup_params)
        format.html { redirect_to @homegroup, notice: 'Homegroup was successfully updated.' }
        format.json { render :show, status: :ok, location: @homegroup }
      else
        format.html { render :edit }
        format.json { render json: @homegroup.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /homegroups/1
  # DELETE /homegroups/1.json
  def destroy
    @homegroup.destroy
    respond_to do |format|
      format.html { redirect_to homegroups_url, notice: 'Homegroup was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_homegroup
      @homegroup = Homegroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def homegroup_params
      params.require(:homegroup).permit(:index, :show)
    end
end
