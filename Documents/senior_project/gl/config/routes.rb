Rails.application.routes.draw do
  resources :homegroups do
    resources :items, shallow: true
  end
  resources :items
  devise_for :users
  root to: 'homegroups#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
