require 'test_helper'

class HomegroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @homegroup = homegroups(:one)
  end

  test "should get index" do
    get homegroups_url
    assert_response :success
  end

  test "should get new" do
    get new_homegroup_url
    assert_response :success
  end

  test "should create homegroup" do
    assert_difference('Homegroup.count') do
      post homegroups_url, params: { homegroup: { index: @homegroup.index, show: @homegroup.show } }
    end

    assert_redirected_to homegroup_url(Homegroup.last)
  end

  test "should show homegroup" do
    get homegroup_url(@homegroup)
    assert_response :success
  end

  test "should get edit" do
    get edit_homegroup_url(@homegroup)
    assert_response :success
  end

  test "should update homegroup" do
    patch homegroup_url(@homegroup), params: { homegroup: { index: @homegroup.index, show: @homegroup.show } }
    assert_redirected_to homegroup_url(@homegroup)
  end

  test "should destroy homegroup" do
    assert_difference('Homegroup.count', -1) do
      delete homegroup_url(@homegroup)
    end

    assert_redirected_to homegroups_url
  end
end
