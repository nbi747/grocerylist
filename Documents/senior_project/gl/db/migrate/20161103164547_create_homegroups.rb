class CreateHomegroups < ActiveRecord::Migration[5.0]
  def change
    create_table :homegroups do |t|
      t.string :name

      t.timestamps
    end
  end
end
