class Homegroup < ApplicationRecord
  has_many :homegroup_users
  has_many :users, through: :homegroup_users
  has_many :items
end
