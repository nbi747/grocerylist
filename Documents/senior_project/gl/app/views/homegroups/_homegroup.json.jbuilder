json.extract! homegroup, :id, :index, :show, :created_at, :updated_at
json.url homegroup_url(homegroup, format: :json)